console.log("Hello")

// [SECTION 1] Assignment operators

// 1. Basic Assignment Operator (=) allows us to add the value of the right
// operand to variable and assigns a result to the variable
 let assignmentNumber = 5; let message = 'This is the message';

console.log("Result of the operation: " + assignmentNumber)
// 2. Addition assignment operator (+=) The addition assignment operator adds
// the value of the right operand to a variable and assigns the result to the
// variable assignmentNumber = assignmentNumber + 2;

// shorthanded version of the statement above
assignmentNumber += 2 
console.log('Result of the operation: ' + assignmentNumber)

// [SECTION 1: SUB 2] Arithmetic Operators(+, -,*, /) 

//3. Subtraction/Multiplication/Division assignment operator (-=)

// assignmentNumber = assignmentNumber - 3
assignmentNumber *= 4

console.log("Result of the operation: " + assignmentNumber) 
let x = 15; 
let y= 10; 
let sum = x + y;

console.log(sum)



let product = x * y; 
console.log(product); //150

// division(/)
let quotient = x/y; 
console.log(quotient)


let remainder = x % y; 
console.log(remainder);

// [SUBSECTION]Multiple Operators and Parentheses when multiple operators are
// applied in a single statement, it follows the PEMDAS Parenthesis,
// exponent, multiplication, , division, addition and subtraction

 let mdas = 1 + 2 - 3 * 4 / 5 
 console.log(mdas)

 // The operations were done in the following order to get to the final
 // answer

 // Note: The order of operations can be changed by adding parentheses to the
 // logic

 let pemdas = 1 + (2-3) * (4/5) 
 console.log(pemdas); 

 // By adding parentheses '()' the order of the operations are changed
 // prioritizing the operations enclosed within parenthesis. This operation
 // was done with the following order: 1. 4/5 = 0.8 and 2 - 3 = -1 2. -1 *
 // 0.8 = -0.8 3. 1+ -0.8 = 0.2


 //[SECTION] Increment and Decrement
 let z = 1;

 //Pre and Post increment (++)

 // pre-increment (syntax: ++variable)
 let preIncrement = ++z;

console.log(preIncrement) //result of the pre-increment console.log(z);

// // the value of "z" is added by a value of 1 before returning the value and
//    storing it inside a new variable:
// "preIncrement" we can see here that the value of z was also increased even
//  though we did not implicitly specify any variable reassignment
console.log(z);

// Post-increment (syntax: variable++)
let postIncrement = z++; 
console.log(postIncrement) 
console.log(z)

// The value of z  is returned and stored inside the variable called "postIncrement."
// the Value of z is at 2 before it was incremented

// pre-increment, add muna yung value, saka irereturn yung bagong value ng variable
// return muna yung value saka iaadd


// Decrement (--)
//Pre-decrement: (syntax: --variableName)
// The value of z starts with 3 before it was decremented
let preDecrement = --z; //3 - original value of z
console.log(preDecrement); //2

//Post-decrement (syntax: variableName--)
let postDecrement = z--
// the value of 'z' is returned and stored inside a new variable before it will be decremented.

console.log(postDecrement);
console.log(z);

let bagongValue = 3;
// let newValue = ++bagongValue; //pre increment
    // console.log('new value using pre-increment:' + newValue); //4

let newValue = bagongValue++
console.log('new value using post-increment:' + newValue)
console.log(newValue)
console.log(bagongValue)

// real life situations in programming where we use this type of operation
// 1.queues - pagproduce
// 2. creating loop conditions

// TYPE COERCION - automatic or implicit conversion of values from one data type to another
// [SECTION:] TYPE COERCION
let numberA = 6;
let numberB = '6';

let coercion = numberA + numberB;
console.log(coercion);
// The two values concatenated with each other giving us a vakue of 66.
// let's check the datatypes of the values above
// typeof expression -> will allow us to identify the data type of a certain value or component
console.log(typeof numberA); //number
console.log(typeof numberB); //string
// number + string --> the number data type was converted into a string to perform concatenation instead of addition
console.log(typeof coercion); //performed string concatenation


// Adding number and boolean
let expressionC = 10 + true;
console.log(expressionC); //11

let a = true
console.log(typeof a); //boolean
let b = 10;
console.log(typeof b); //number

// note: the boolean value of "true is also associated with the value of 1"

let expressionE = 10 + false;
console.log(expressionE);

// the boolean value of 'false' is associated with a value of 0

let cvv = true + false;
console.log(cvv)

//Number with a null value
let expressionG = 8 + null; 
console.log(expressionG);

let d = null;
console.log(typeof d);

//Conversion rules:
//1. If atleast one operand is an object, it will be converted into a primitive value / data type. 
//2. After conversion, if atleast 1 operand is a string data type, the second operand is converted into another string to perform concatenation.
//3. In other cases where both operands are converted to numbers then an arithmetic operation is executed
//primitive: number, strings, boolean ---> they can only execute one value


// String with a null data type
let expressionH = "Batch145" + null
console.log(expressionH)
// "Batch145" + "null" = "Batch145null" [rules 1 and 2 apply]
// 1. Batch145 -> string data type //null will be converted into a primitive data type
//composite vs primitive ---> composite can store multiple values

//Number with undefined 

expressionH = 9 + undefined; //NaN --> Not a Number
console.log(expressionH);

let e = undefined;
console.log(typeof e)
//1. 9 = number; undefined was converted into a number data type NaN --> rule #3
//2. 9 + NaN = NaN 

//COMPARISON OPERATORS - checks if two values are the same or not
//EQUALITY OPERATORS - checks whether the operands are equal/have the same content

//[SECTION] Comparison Operators
let name = 'Juan'
//[SUB SECTION] EQUALITY OPERATORS (==)
//attempts to CONVERT and COMPARE operands with 2 different data types
//returns a boolean value 
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1') //true
console.log(1 == true)//true
console.log(1 == false); //false
console.log(name == 'Juan'); //true ---kasi nagdeclare sya ng variable name
console.log('Juan' == 'juan'); //false
// console.log('Juan' == Juan) //error because the variable was not declared.

//[SUB SECTION] Inequality Operator (!=)

//-> checks whether the operands are NOT EQUAL / HAVE DIFFERENT VALUES
console.log(1 != 1) //false
console.log(1 != 2) //true
console.log(1 != '1') //false
console.log(0 != false) //false
let juan = 'juan';
console.log('juan' != juan) //false

// [SUBSECTION] "STRICT" EQUALITY OPERATORS (===)
//-> checks whether the operands are equal or have the same value. 
//-> Also compares if the data types are the same.

console.log(1 === 1); //true  - same value same data types
console.log(1 === '1'); //false - different data types 
console.log(0 === false); //false - same value different data types

//they have different data types hence, false.

//[SUBSECTION] "Strict" INEQUALITY OPERATOR (!==)
//-> this will check if the operands are NOT EQUAL / HAVE DIFFERENT values/content.

//-> checks both values and data types of both components and operands

console.log(1 !== 1); //false --> same value same data type
console.log(1 !== 2); //true --> diff value same data type
console.log(1 !== '1'); //true --> same value different data type
console.log(0 !== false); //true --> same value different data type

//Developer's tip: Upon creating conditions or statement, it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.

//[SECTION] Relational Operators
//compare if the operands are greater or lesser than the other
let priceA = 1800;
let priceB = 1450;

//lesser than operator
console.log(priceA < priceB); //false
//greater than operator
console.log(priceA > priceB); //true

let expressionI = 150 <= 150; //true

//DEVELOPER's TIP: When writing down / selecting variable name that would describe / contain a boolean value. It is a writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.
// isSingle = true //denotes and distinguishes yes or no value that is being answered 

isLegalAge = true;
isRegistered = false;
//for the person to be able to vote, both requirements has to be met
//we need to use the proper logical operator to determine if allowed to vote
// AND (&& Double ampersand) all criteria has to be met
let isAllowedToVote = isLegalAge && isRegistered;
console.log('is the person allowed to vote? ' + isAllowedToVote);

//OR (|| Double pipe) atleast 1 criteria has to be met in order to pass
let isAllowedforVaccination = isLegalAge || isRegistered;
console.log('Did the person pass? ' + isAllowedforVaccination);

//NOT (! - Exclamation point) OPERATOR
//This will convert/return the opposite value 
let isTaken = true; 
let isTalented = false;
console.log(!isTaken);