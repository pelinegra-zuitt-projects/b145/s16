console.log('Hello World')

// Create two variables that will store two different number values and create
// several variables that will store the sum, difference, product and
// quotient of the two numbers. Print the results in the console

let varA = 5 
let varB = 20

let sum = varA + varB
let difference = varA - varB
let product = varA * varB
let quotient = varA / varB

console.log('The sum of the two numbers is: ' + sum);
console.log('The difference of the two numbers is: ' + difference);
console.log('The product of the two numbers is: ' + product);
console.log('The quotient of the two numbers is: ' + quotient);


// 4. Using comparison operators print out a string in the console that will
// return a message if the sum is greater than the difference.

let x = 67
let y = 78

let sum2 = x + y
let difference2 = x - y
let Happy = sum2>y
console.log('The sum is greater than the difference: ' + Happy)


 
// 5. Using comparison and logical operators print out a string in the console
// that will return a message if the product and quotient are both positive
// numbers. 

let haha = 3
let hehe = 15
producthahe = haha * hehe
quotienthahe = haha / hehe
positivproducthahe = producthahe > 0
positivquotienthahe = quotienthahe > 0
console.log('The product and quotient are both positive: ' + (positivproducthahe&&positivquotienthahe));

// 6. Using comparison and logical operators print out a string in the console
// that will return a message if one of the results is a negative number.

let huhu = -1
let hoho = 20
let coconegative = (huhu * hoho) < 0
let cucunegative = (huhu + hoho) < 0
let isOnenegative = coconegative || cucunegative;

console.log('One of the results is negative: ' + isOnenegative);

// 7. Using comparison and logical operators print out a string in the console
// that will return a message if one of the results is zero.

let harhar = 7
let hook = 30
let varharhar = ((harhar * hook)!==0)
let varhook = ((harhar / hook)!==0)

let isallEqualtozero =(varharhar || varhook);

console.log('All the results are not equal to zero: ' + isallEqualtozero);
